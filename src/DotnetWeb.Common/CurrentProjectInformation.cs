using System.Collections.Generic;

namespace DotnetWeb.Common
{
    public class CurrentProjectInformation
    {
        public const string ApiVersion1 = "1";

        public static List<string> All() => new List<string>
        {
            ApiVersion1
        };

        public static string CurrentApiVersion => ApiVersion1;

        public string ApiVersion => CurrentApiVersion;
        public string ProjectVersion { get; set; }
        public string GitCommitHash { get; set; }
    }
}