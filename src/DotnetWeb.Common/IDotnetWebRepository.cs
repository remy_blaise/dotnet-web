using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotnetWeb.Common
{
    public interface IDotnetWebRepository
    {
        Task<string> Get(string param);
        Task<IEnumerable<string>> List();
    }
}