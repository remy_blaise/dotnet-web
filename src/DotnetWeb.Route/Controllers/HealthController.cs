using DotnetWeb.Common;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers
{
    [ApiController]
    [ApiVersionNeutral]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class HealthController : Controller
    {
        private readonly CurrentProjectInformation _currentProjectInformation;
        public HealthController(CurrentProjectInformation currentProjectInformation)
        {
            _currentProjectInformation = currentProjectInformation;
        }

        /// <summary>
        /// Health Check Endpoint.
        /// </summary>
        /// <returns>Returns a success statement</returns>
        /// <response code="200">All Good</response>        
        [ProducesResponseType(200)]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_currentProjectInformation);
        }
    }
}
