using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotnetWeb.Common;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Route.Controllers
{
    [ApiController]
    [ApiVersion(CurrentProjectInformation.ApiVersion1)]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DotnetWebController : Controller
    {
        private readonly IDotnetWebService _service;
        private readonly ILogger<DotnetWebController> _logger;

        public DotnetWebController(IDotnetWebService service, ILogger<DotnetWebController> logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Lists all Items.
        /// </summary>
        /// <returns>A list of all items</returns>
        /// <response code="200">Returns the item list</response>
        /// <response code="204">If the items were not found</response>             
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogDebug("List Controller Hit");
            var model = await _service.List();
            if (model != null)
            {
                return Ok(model);
            }

            return NoContent();
        }

        /// <summary>
        /// Fetches a specific Item.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A the newly created item</returns>
        /// <response code="200">Returns the item that was created</response>
        /// <response code="404">If the item was not found</response>             
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            _logger.LogDebug("Get Controller Hit");
            var model = await _service.Get(id);
            if (model != null)
            {
                return Ok(model);
            }

            return NotFound();
        }

        /// <summary>
        /// Creates an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/{version}/DotnetWeb
        ///     {
        ///        "name": "Item1"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been created</response>       
        [ProducesResponseType(201)]
        [HttpPost]
        public IActionResult Post([FromBody]string item)
        {
            _logger.LogDebug("Post Controller Hit");
            return Created($"{Request.GetDisplayUrl()}/{item}", item);
        }

        /// <summary>
        /// Update an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/{version}/DotnetWeb
        ///     {
        ///        "id": 1,
        ///        "name": "Item1"
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been updated</response>       
        [ProducesResponseType(201)]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]string item)
        {
            _logger.LogDebug("Put Controller Hit");
            return Accepted();
        }

        /// <summary>
        /// Delete an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/{version}/DotnetWeb
        ///     {
        ///        "id": 1
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been deleted</response>       
        [ProducesResponseType(201)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _logger.LogDebug("Delete Controller Hit");
            return Accepted();
        }
    }
}
