using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DotnetWeb.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.AzureFunction
{
    public class HttpFunction
    {
        [FunctionName("HttpFunction")]
        public IActionResult Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]
            HttpRequest httpRequest,
            ILogger logger)
        {
            if (CanParseBody(httpRequest, out var parsed))
            {
                var service = httpRequest.HttpContext.RequestServices.GetService<IDotnetWebService>();
                service.Get(parsed.FirstOrDefault());
                return new OkResult();
            }

            return new BadRequestObjectResult("Could not parse request");
        }

        private static bool CanParseBody(HttpRequest httpRequest, out List<string> val)
        {
            try
            {
                var requestBody = new StreamReader(httpRequest.Body).ReadToEnd();
                if (!string.IsNullOrWhiteSpace(requestBody))
                {
                    var unescaped = System.Text.RegularExpressions.Regex.Unescape(requestBody);
                    val = new List<string>
                    {
                        unescaped
                    };

                    return true;
                }

                val = new List<string>();
                return false;
            }
            catch (Exception)
            {
                val = new List<string>();
                return false;
            }
        }
    }
}
