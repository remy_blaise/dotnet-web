using System.IO;
using DotnetWeb.AzureFunction;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Common;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

[assembly: FunctionsStartup(typeof(Startup))]
namespace DotnetWeb.AzureFunction
{
    internal class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            var currentProjectInformation = GetConfigurationRoot()
                .GetSection("CurrentProjectInformation")
                .Get<CurrentProjectInformation>();

            builder.Services.AddSingleton(currentProjectInformation);

            var services = new BootstrapperRegistration().Register(builder.Services);

            builder.Services.Add(services);
        }

        private static IConfigurationRoot GetConfigurationRoot()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

    }
}