using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DotnetWeb.Bootstrapper;
using Autofac;
using DotnetWeb.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace DotnetWeb.ConsoleApplication
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var logger = LogManager.LoadConfiguration("nlog.config").GetCurrentClassLogger();
            var configuration = GetConfigurationRoot();

            try
            {
                var runner = new DotnetWebRunner();
                using (var scope = Container(configuration).BeginLifetimeScope())
                {
                    if (CanParseBody(args, out var parsed))
                    {
                        runner.Run(scope, parsed.ToArray());
                    }
                    else
                    {
                        var environment = scope.Resolve<IHostingEnvironment>();

                        if (!EnvironmentName.Development.Equals(environment.EnvironmentName))
                        {
                            return;
                        }

                        var cliArgs = string.Empty;
                        while (!Contains(cliArgs, QuitCommands()))
                        {
                            Console.WriteLine("Enter Command: ");
                            cliArgs = Console.ReadLine();
                            if (CanParseBody(cliArgs, out var parsedCli))
                            {
                                runner.Run(scope, parsedCli.ToArray());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        private static IContainer Container(IConfigurationRoot configuration)
        {
            var currentProjectInformation = configuration
                .GetSection("CurrentProjectInformation")
                .Get<CurrentProjectInformation>();

            var services = new ServiceCollection()
                .AddSingleton(currentProjectInformation)
                .AddSingleton(GetEnvironment());

            return new BootstrapperRegistration()
                .RegisterAutofac(services
                    .AddLogging(loggingBuilder =>
                    {
                        loggingBuilder.ClearProviders();
                        loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                        loggingBuilder.AddNLog(configuration);
                    }))
                .Build();
        }

        private static IHostingEnvironment GetEnvironment()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (EnvironmentName.Development.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new DotnetWebEnvironment(EnvironmentName.Development);
            }

            if (EnvironmentName.Staging.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new DotnetWebEnvironment(EnvironmentName.Staging);
            }

            return new DotnetWebEnvironment(EnvironmentName.Production);
        }

        private class DotnetWebEnvironment : IHostingEnvironment
        {
            public DotnetWebEnvironment(string environmentName)
            {
                EnvironmentName = environmentName;
            }

            public string EnvironmentName { get; set; }
            public string ApplicationName { get; set; }
            public string WebRootPath { get; set; }
            public IFileProvider WebRootFileProvider { get; set; }
            public string ContentRootPath { get; set; }
            public IFileProvider ContentRootFileProvider { get; set; }
        }

        private static IConfigurationRoot GetConfigurationRoot()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static bool CanParseBody(string arg, out IList<string> val)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                val = new List<string>();
                return false;
            }

            return CanParseBody(new[] { arg }, out val);
        }

        private static bool CanParseBody(string[] args, out IList<string> val)
        {
            try
            {
                if (args != null && args.Any())
                {
                    val = args;
                    return true;
                }

                val = new List<string>();
                return false;
            }
            catch (Exception)
            {
                val = new List<string>();
                return false;
            }
        }

        private static bool Contains(string val, IList<string> checkList)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return false;
            }

            return checkList.Any(i => i.Equals(val, StringComparison.OrdinalIgnoreCase));
        }

        private static IList<string> QuitCommands() => new List<string>
        {
            "q"
        };
    }
}
