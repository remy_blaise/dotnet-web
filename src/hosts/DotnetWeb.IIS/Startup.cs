using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DotnetWeb.IIS
{
    public class Startup
    {
        public IHostingEnvironment HostingEnvironment { get; }
        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public Startup(IConfiguration config, IHostingEnvironment env)
        {
            HostingEnvironment = env;
            Configuration = config;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var settingsSection = Configuration.GetSection("CurrentProjectInformation");
            var currentProjectInformation = settingsSection.Get<CurrentProjectInformation>();

            services.AddSingleton(currentProjectInformation);
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddControllersAsServices();

            services.AddApiVersioning(option =>
            {
                option.ReportApiVersions = true;
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.DefaultApiVersion = new ApiVersion(int.Parse(CurrentProjectInformation.CurrentApiVersion), 0);
            });

            services.AddSwaggerGen(c =>
            {
                foreach (var apiVersion in CurrentProjectInformation.All())
                {
                    c.SwaggerDoc($"v{apiVersion}", GetOpenApiDocument(apiVersion));
                }

                c.DocInclusionPredicate((docName, apiDesc) => true);
                c.OperationFilter<RemoveVersionParameters>();
                c.DocumentFilter<SetVersionInPaths>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml".Replace("IIS", "Route");
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath, true);
            });

            ApplicationContainer = new BootstrapperRegistration()
                .RegisterAutofac(services)
                .Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }

        private static Info GetOpenApiDocument(string apiVersion) => new Info
        {
            Version = $"v{apiVersion}",
            Title = $"DotnetWeb API V{apiVersion}"
        };

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IConfiguration config)
        {
            app.UseMvc();
            app.UseSwagger();
            app.UseApiVersioning();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = string.Empty;
                    foreach (var apiVersion in CurrentProjectInformation.All())
                    {
                        c.SwaggerEndpoint($"/swagger/v{apiVersion}/swagger.json", $"DotnetWeb V{apiVersion}");
                    }

                });
            }
            else
            {
                app.UseHttpsRedirection();
            }
        }
    }

    public class SetVersionInPaths : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context) => swaggerDoc.Paths = swaggerDoc.Paths.ToDictionary(entry => entry.Key.Replace("v{version}", swaggerDoc.Info.Version), entry => entry.Value);
    }

    public class RemoveVersionParameters : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context) => operation.Parameters.Remove(operation.Parameters.Single(p => p.Name == "version"));
    }
}