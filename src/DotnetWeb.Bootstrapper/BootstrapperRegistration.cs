using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace DotnetWeb.Bootstrapper
{
    public class BootstrapperRegistration
    {
        public ContainerBuilder RegisterAutofac(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new DotnetWebAutofacModule());
            return builder;
        }

        public IServiceCollection Register(IServiceCollection services) => new ServiceCollection
        {
            services,
            Registrations.Load()
        };
    }

    public class DotnetWebAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder) => builder.Populate(Registrations.Load());
    }
}
