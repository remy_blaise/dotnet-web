using System.Collections.Generic;
using DotnetWeb.Common;
using DotnetWeb.Core;
using DotnetWeb.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper
{
    public class Registrations
    {
        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.2#service-lifetimes
        public static IList<ServiceDescriptor> Load()
        {
            var services = new ServiceCollection();

            services.AddTransient<IDotnetWebRepository, DotnetWebRepository>();
            services.AddTransient<IDotnetWebService, DotnetWebService>();

            return services;
        }
    }
}