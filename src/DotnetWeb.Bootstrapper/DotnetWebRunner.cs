using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using DotnetWeb.Common;

namespace DotnetWeb.Bootstrapper
{
    public class DotnetWebRunner
    {
        public void Run(ILifetimeScope scope, IList<string> args)
        {
            var dotnetWebService = scope.Resolve<IDotnetWebService>();

            using (var lifetime = scope.BeginLifetimeScope())
            {
                if (CanParseBody(args, out var parsed))
                {
                    var task = dotnetWebService.Get(parsed.FirstOrDefault());

                    Task.WaitAll(task);

                    Console.WriteLine(task.Result);
                }
                else
                {
                    Console.WriteLine("Could not parse command");

                }
            }

        }

        private static bool CanParseBody(IList<string> args, out IList<string> val)
        {
            try
            {
                if (args != null && args.Any())
                {
                    val = args;
                    return true;
                }

                val = new List<string>();
                return false;
            }
            catch (Exception)
            {
                val = new List<string>();
                return false;
            }
        }
    }
}