using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWeb.Common;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Repository
{
    public class DotnetWebRepository : IDotnetWebRepository
    {
        private readonly ILogger<DotnetWebRepository> _logger;

        public DotnetWebRepository(ILogger<DotnetWebRepository> logger)
        {
            _logger = logger;
        }

        public Task<string> Get(string param)
        {
            _logger.LogDebug("Get Repository Hit");
            return Task.FromResult(param);
        }

        public Task<IEnumerable<string>> List()
        {
            _logger.LogDebug("List Repository Hit");
            var list = new List<string>
            {
                "Hello",
                "World",
                "!"
            };

            return Task.FromResult(list.AsEnumerable());
        }
    }
}