using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWeb.Common;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Core
{
    public class DotnetWebService : IDotnetWebService
    {
        private readonly ILogger<DotnetWebService> _logger;
        private readonly IDotnetWebRepository _repository;

        public DotnetWebService(IDotnetWebRepository repository, ILogger<DotnetWebService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<string> Get(string param)
        {
            _logger.LogDebug("Get Service Hit");
            Validate(param);

            return await _repository.Get(param);
        }

        public async Task<IEnumerable<string>> List()
        {
            _logger.LogDebug("List Service Hit");
            var list = await _repository.List();

            return list == null ? new List<string>() : list.DefaultIfEmpty();
        }

        private void Validate(string param)
        {
            if (!string.IsNullOrWhiteSpace(param))
            {
                return;
            }

            throw new Exception("Missing param");
        }
    }
}
