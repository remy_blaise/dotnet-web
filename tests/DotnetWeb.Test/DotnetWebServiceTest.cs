using System;
using System.Threading.Tasks;
using DotnetWeb.Common;
using DotnetWeb.Core;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace DotnetWeb.Test
{
    [TestFixture]
    public class DotnetWebServiceTest
    {
        private IDotnetWebService _service;
        private IDotnetWebRepository _repository;
        private ILogger<DotnetWebService> _logger;

        [SetUp]
        public void Setup()
        {
            _repository = Substitute.For<IDotnetWebRepository>();
            _logger = Substitute.For<ILogger<DotnetWebService>>();
            _service = new DotnetWebService(_repository, _logger);
        }

        [Test]
        public void GetWithValidInputReturnsValue()
        {
            //arrange
            var random = new Random();
            var param = random.Next(0, 1000).ToString();
            var expected = random.Next(0, 1000).ToString();

            _repository.Get(param).Returns(Task.FromResult(expected));

            //act
            var result = _service.Get(param);

            //assert
            _repository.Received(1).Get(param);
            Assert.That(result.Result, Is.EqualTo(expected));
        }
    }
}